#!/bin/sh

set -e

scripts=(calc_tickadj checktime freq_adj ntp-groper ntp-status ntp-wait ntpsweep ntptrace summary);

for script in ${scripts[@]} 
do
    def="$script-opts.def"
    base=`basename $def .def`
    BASE=`echo $base | tr a-z- A-Z_`
    autogen -L ../../autogen -L ../sntp/include -L ../sntp/ag-tpl $def
    autogen -L ../../autogen -L ../sntp/include -L ../sntp/ag-tpl \
        -DMAN_SECTION=1ntp-waitman -Tagman-cmd.tpl $def

    if grep -qi "autogen definitions options" $def;then
        cflags="-DTEST_${BASE} `autoopts-config cflags`"
        ldflags="`autoopts-config ldflags`"
        cc -o ${base} -g ${cflags} ${base}.c ${ldflags}
    fi
done
