use strict;
use warnings;
use Test::More tests => 1;
use Test::Command;

my $script_name = 'checktime';

cmd_is("$script_name -?", '', qr/-l, --host-list/, 0, 
    'help mentions -h/--host-list');
