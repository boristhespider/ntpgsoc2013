use strict;
use warnings;
use Test::More tests => 21;
use Test::Command;

my @script_list = qw(calc_tickadj checktime freq_adj);

for my $script (@script_list) {
    fail("$script not a regular file") if (!-f $script);
    pass("$script is a regular file");
    fail("$script not an executabe") if (!-x $script);
    pass("$script is an executable");
    fail("$script is not readable") if (!-r $script);
    pass("$script is readable");

    TODO: {
        todo_skip "Broken script", 4 if $script eq 'freq_adj';

        cmd_is("$script", undef, undef, undef, "$script executes");
        cmd_is("$script --rubbish", '', qr/^Unknown option:/, 1, 
            "$script reports unkown option");
        cmd_is("$script -?", '', qr/^$script -/, 0, 
            "$script support -?");
        cmd_is("$script --help", '', qr/^$script -/, 0, 
            "$script supports --help");
    }
}
