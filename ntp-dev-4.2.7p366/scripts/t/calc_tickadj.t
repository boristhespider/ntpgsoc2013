use strict;
use warnings;
use Test::More tests => 3;
use Test::Command;

my $script_name = 'calc_tickadj';

cmd_is("$script_name -?", '', qr/-d, --drift-file/, 0, 
    "help mentions -d/--drift-file");
cmd_is("$script_name -d t/data/test.drift", undef, '', 0, 
    "accepts argument to -d");
cmd_is("$script_name --drift-file t/data/test.drift", undef, '', 0, 
    "accepts argument to --drift-file");
