use strict;
use warnings;
use Test::More tests => 4;
use Test::Command;

my $script_name = 'freq_adj';

cmd_is("$script_name -?", '', qr/-t, --timer/, 0, 
    'help mentions -t/--timer');
cmd_is("$script_name -?", '', qr/-n, --no-op/, 0, 
    "help mentions -n/--no-op");
cmd_is("$script_name -?", '', qr/-n, --no-op/, 0, 
    "help mentions -n/--no-op");
cmd_is("$script_name -?", '', qr/-d, --drift-file/, 0, 
    'help mentions -d/--drift-file');
