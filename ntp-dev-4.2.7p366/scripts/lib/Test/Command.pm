package Test::Command;
use strict;
use warnings;
use Test::Builder;
use IPC::Open3;
use IO::Handle;
use Carp;
use base 'Exporter';
our @EXPORT = qw(cmd_is);
our $VERSION = 0.1;

my $Test = Test::Builder->new();

sub cmd_is {
    my ($cmd, $ex_stdout, $ex_stderr, $ex_ret, $descr) = @_;

    croak "Specify a command to cmd_is" unless $cmd;

    my ($pid, $writer_h, $stdout_h, $stderr_h) = 
        (0, IO::Handle->new(), IO::Handle->new(), IO::Handle->new());

    eval { $pid = open3($writer_h, $stdout_h, $stderr_h, $cmd); };
    if ($@) {
        $Test->diag($!);
        $Test->ok(0, $descr);
        return;
    }

    my $stdout = do { local $/; <$stdout_h> };
    my $stderr = $stderr_h ? do { local $/; <$stderr_h> } : '';
    my $ok = 1;

    if (defined $ex_stdout && !_test_string($stdout, $ex_stdout)) {
        $ok = 0;
        $Test->diag(<<DIAG);
STDOUT doesn't match
Expected: '$ex_stdout'
Got: '$stdout'
DIAG
    }
    if (defined $ex_stderr && !_test_string($stderr, $ex_stderr)) {
        $ok = 0;
        $Test->diag(<<DIAG);
STDERR doesn't match
Expected: '$ex_stderr'
Got: '$stderr'
DIAG
    }

    waitpid $pid, 0;
    my $ret =  $? >> 8;

    if (defined $ex_ret && $ret != $ex_ret) {
        $ok = 0;
        $Test->diag(<<DIAG);
Return value doesn't match
Expected: $ex_ret
Got: $ret
DIAG
    }

    $Test->ok($ok, $descr);
}

sub _test_string {
    my ($got, $exp) = @_;
    if (ref $exp eq 'Regexp') {
        return $got =~ $exp;
    }
    else {
        return $got eq $exp;
    }
}

1;
__END__
=head1 NAME

Test::Command - Test output and ret value of an external command.

=head1 VERSION

Version 0.1

=head1 SYNOPSIS

    use Test::More tests => 1;
    use Test::Command;

    cmd_is('ls', '.\n..', '', 0, "ls on empty directory");

=head1 DESCRIPTION

Test::Command provides a simple interface for testing stdout, stderr and return
value of an external command.

=cut

=head1 TESTS

=over 4 

=item B<is_cmd(cmd, exp_stdout, exp_stderr, exp_ret, diag)>

    Runs command and tests for expected results. Pass undef in the paramter you
    don't want to test for.

=over 4 

=item B<cmd> - string of command tu run

=item B<exp_stdout> - expected stdout. Can be string or compiled regexp

=item B<exp_stderr> - expected stderr. Can be string or compiled regexp

=item B<exp_ret> - expected return value of a program

=item B<diag> - diagnostic message printed if test failed

=back

=back

=head1 AUTHOR

Oliver Kindernay <oliver.kindernay@gmail.com>

=cut
