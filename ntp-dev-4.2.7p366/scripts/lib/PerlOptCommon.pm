package PerlOptCommon;
use strict;
use warnings;
use Getopt::Long qw(GetOptionsFromArray);

Getopt::Long::Configure(qw(no_auto_abbrev no_ignore_case_always));

{
    my $usage;

    sub _gen_usage {
        my ($prog, $opts) = @_;

        $usage = <<USAGE_TOP;
$prog->{name} - $prog->{title}
USAGE: $prog->{name} [ -<flag> [<val>] | --<name>[{=| }<val>] ]... $prog->{argument}

USAGE_TOP

        for my $opt (@$opts) {
            next if $opt->{deprecated};

            my $optcomment = '';
            $optcomment = 'may appear multiple times'
                if (ref $opt->{target} eq "ARRAY");

            $usage .= sprintf '    %2s, --%-20s %s'.
                ($optcomment ? '%s%60.32s' : '')."\n", 
                    ($opt->{short} ? '-'.$opt->{short} : ' '), 
                    $opt->{name}.
                    ($opt->{argname} ? '='.$opt->{argname} : ''), 
                    $opt->{dscr}, 
                    ($optcomment ? ("\n", '- '.$optcomment) : () );
        }

        $usage .= <<USAGE_BOTTOM;

Options are specified by doubled hyphens and their name or by a single
hyphen and the flag character.
USAGE_BOTTOM

    }

    sub _get_usage {
        $usage = _gen_usage(@_) if not defined $usage;
        return $usage;
    }
}

sub processOptions {
    my ($prog, $args, $opts) = @_;
    my (@defs, $popts);

    my $usage = _get_usage($prog, $opts);

    @defs = map { 
        $_->{name}.($_->{short} ? '|'.$_->{short} : '').$_->{arg}
    } @$opts;

    $popts = { map { $_->{name} => $_->{target} } @$opts };

    my $ret = GetOptionsFromArray($args, $popts, @defs);
    usage(0, $usage) if ($popts->{'help'});
    paged_usage(0, $usage) if ($popts->{'more-help'});

    if ($prog->{argument} && $prog->{argument} =~ /^[^\[]/ && !@$args) {
        print STDERR "Not enough arguments supplied (See --help/-?)\n";
        exit 1;
    }
    return ($ret, $popts);
}

sub usage {
    my $ret = shift;
    print STDERR _get_usage();
    exit $ret;
}

sub paged_usage {
    my $ret = shift;
    my $pager = $ENV{PAGER} || '(less || more)';

    open STDOUT, "| $pager"  or die "Can't fork a pager: $!";
    print _get_usage();
}

END { close STDOUT };

1;
