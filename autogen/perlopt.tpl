[= AutoGen5 template pm=(begin 
    (define perl-package (string-tr! (base-name) "-" "_"))
    (string-append perl-package ".pm")
) =]
[= (dne "# " "# ") =][=
(if (not (and (exist? "prog-name") (exist? "prog-title")))
    (error "prog-name and prog-title are required"))
(define prog-name (get "prog-name"))
(if (> (string-length prog-name) 16)
    (error (sprintf "prog-name limited to 16 characters:  %s"
           prog-name)) ) 
(if (not (exist? "long-opts"))
    (error "long-opts is required"))
=]

package [= (emit perl-package) =];
use strict;
use warnings;
use PerlOptCommon;
use Exporter qw(import);
our @EXPORT = qw(processOptions usage);

# Construct hash describing parameters
my @opts = (
[= FOR flag =][=

(define optname-from "A-Z_^")
(define optname-to   "a-z--")

(define optarg "")
(define opttarget "''")
(define optargname "")
(define optcomment "") 
=][=
IF arg-type =][= 
    CASE arg-type =][= 
    =* num =][= (set! optarg "=i") =][= 
    =* str =][= (set! optarg "=s") =][= 
    * =][= 
        (error (string-append "unknown arg type '" 
        (get "arg-type") "' for " (get "name"))) =][= 
        ESAC arg-type =][= 
ENDIF =][=

(if (exist? "stack-arg") 
    (if (and (exist? "max") (== (get "max") "NOLIMIT"))
        (set! opttarget (string-append 
            "[" 
            (if (exist? "arg-default") 
                (string-append "'" (get "arg-default") "'") "")
            "]"
        ))
        (error "If stack-arg then max has to be NOLIMIT")
    )
    (if (exist? "arg-default") (set! opttarget 
        (string-append "'" (get "arg-default") "'"))
    )
)

(if (exist? "arg-type") (set! optargname (substring (get "arg-type") 0 3)))
=]{
    name       => '[= (string-tr! (get "name") optname-from optname-to) =]',
    short      => '[= value =]',
    arg        => '[= (emit optarg) =]',
    target     => [= (emit opttarget) =],
    dscr       => '[= descrip =]',
    argname    => '[= (emit optargname) =]',
    deprecated => [= (if (exist? "deprecated") 1 0) =],
},
[= ENDFOR 
=]{
    name       => 'help',
    short      => '?',
    arg        => '',
    argname    => '',
    target     => '',
    dscr       => 'Display usage information and exit',
    deprecated => 0,
},
{
    name       => 'more-help',
    short      => '',
    arg        => '',
    argname    => '',
    target     => '',
    dscr       => 'Pass the extended usage information through a pager',
    deprecated => 0,
},
);

my %prog = (
    name     => '[= prog-name =]',
    title    => '[= prog-title =]',
    argument => '[= argument =]',
);

sub processOptions {
    my $args = shift;
    my ($ret, $popts) = PerlOptCommon::processOptions(\%prog, $args, \@opts);
    $_[0] = $popts;
    return $ret;
}

sub usage {
    PerlOptCommon::usage(@_);
}

sub paged_usage {
    PerlOptCommon::paged_usage(@_);
}


1;
