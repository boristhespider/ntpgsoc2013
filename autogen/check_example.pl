#!/usr/bin/perl

use strict;
use warnings;
use check_opts;

my $opts = {};
if (!processOptions($opts, @ARGV)) {
    usage(1);
};

use Data::Dumper;
#print Dumper($opts);
